import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import matplotlib.tri as mplt

#mass
m = 3.14
#major radius
c = 4.0
#minor radius
a = 1.0
#principal moments of inertia
I1 = m*((5.0/8.0)*a**2.0 + (1.0/2.0)*c**2.0)
I2 = m*((5.0/8.0)*a**2.0 + (1.0/2.0)*c**2.0)
I3 = m*((3.0/4.0)*a**2.0 + c**2.0)

#time data
cycles = 300
t = np.arange(cycles, dtype='float64')
endt = 16.0
t[cycles-1] = endt;
#step size
h = endt/cycles

#angular velocity
wx = np.arange(cycles, dtype='float64')
wy = np.arange(cycles, dtype='float64')
wz = np.arange(cycles, dtype='float64')
wx[0] = 1.0
wy[0] = 1.0
wz[0] = 1.0

#arrays to hold runge kutta steps
k1 = np.arange(3, dtype='float64')
k2 = np.arange(3, dtype='float64')
k3 = np.arange(3, dtype='float64')
k4 = np.arange(3, dtype='float64')

#constants in euler equations
L1 = (I3-I2)/I1
L2 = (I1-I3)/I2
L3 = (I2-I1)/I3

#record changes in omega over time using 4th order runge kutta
for i in range(0, cycles - 1):
	k1[0] = -h * L1 * wy[i] * wz[i]
	k1[1] = -h * L2 * wx[i] * wz[i] 
	k1[2] = -h * L3 * wx[i] * wy[i]
	
	k2[0] = -h * L1 * (wy[i] + k1[1]/2.0) * (wz[i] + k1[2]/2.0) 
	k2[1] = -h * L2 * (wx[i] + k1[0]/2.0) * (wz[i] + k1[2]/2.0) 
	k2[2] = -h * L3 * (wx[i] + k1[0]/2.0) * (wy[i] + k1[1]/2.0) 

	k3[0] = -h * L1 * (wy[i] + k2[1]/2.0) * (wz[i] + k2[2]/2.0) 
	k3[1] = -h * L2 * (wx[i] + k2[0]/2.0) * (wz[i] + k2[2]/2.0) 
	k3[2] = -h * L3 * (wx[i] + k2[0]/2.0) * (wy[i] + k2[1]/2.0) 

	k4[0] = -h * L1 * (wy[i] + k3[1]) * (wz[i] + k3[2]) 
	k4[1] = -h * L2 * (wx[i] + k3[0]) * (wz[i] + k3[2]) 
	k4[2] = -h * L3 * (wx[i] + k3[0]) * (wy[i] + k3[1]) 

	wx[i+1] = wx[i] + (k1[0]/6 + 2*k2[0]/3 + 2*k3[0]/3 + k4[0]/6)
	wy[i+1] = wy[i] + (k1[1]/6 + 2*k2[1]/3 + 2*k3[1]/3 + k4[1]/6)
	wz[i+1] = wz[i] + (k1[2]/6 + 2*k2[2]/3 + 2*k3[2]/3 + k4[2]/6)
	t[i] = i*h

#create mesh
u = 0
v = 0
resolution = 12
step = (2.0*np.pi)/resolution
x = []
y = []
z = []
indices = []
indxCount = 0
for i in range(0, resolution):
	for j in range(0, resolution):
		#triangle 1 + triangle 2 = quad
		#triangle 1
		x.append((c + a * np.cos(v+step)) * np.cos(u))
		y.append((c + a * np.cos(v+step)) * np.sin(u))
		z.append(a * np.sin(v+step))

		x.append((c + a * np.cos(v)) * np.cos(u))
		y.append((c + a * np.cos(v)) * np.sin(u))
		z.append(a * np.sin(v))

		x.append((c + a * np.cos(v)) * np.cos(u+step))
		y.append((c + a * np.cos(v)) * np.sin(u+step))
		z.append(a * np.sin(v))

		indices.append([indxCount, indxCount + 1, indxCount + 2])
		indxCount +=3

		#triangle 2
		x.append((c + a * np.cos(v)) * np.cos(u+step))
		y.append((c + a * np.cos(v)) * np.sin(u+step))
		z.append(a * np.sin(v))

		x.append((c + a * np.cos(v+step)) * np.cos(u+step))
		y.append((c + a * np.cos(v+step)) * np.sin(u+step))
		z.append(a * np.sin(v+step))

		x.append((c + a * np.cos(v+step)) * np.cos(u))
		y.append((c + a * np.cos(v+step)) * np.sin(u))
		z.append(a * np.sin(v+step))

		indices.append([indxCount, indxCount + 1, indxCount + 2])
		indxCount +=3

		u+=step
	u = 0
	v += step

#draw the graph
fig = plt.figure()
ax = fig.add_subplot(111, projection = '3d')
ax.set_title('Torus')
ax.set_xlabel('x (m)')
ax.set_ylabel('y (m)')
ax.set_zlabel('z (m)')
axlim = 7
ax.set_xlim(-axlim,axlim)
ax.set_ylim(-axlim,axlim)
ax.set_zlim(-axlim,axlim)
ax.plot_trisurf(x, y, z, triangles = np.asarray(indices))
plt.grid()
ax.view_init(45, 45)

for i in range (0, cycles):	
	#create rotation matrix
	theta = np.sqrt(wx[i]**2+wy[i]**2+wz[i]**2)*t[i]

	A = wx[i]/np.sqrt(wx[i]**2+wy[i]**2+wz[i]**2)
	B = wy[i]/np.sqrt(wx[i]**2+wy[i]**2+wz[i]**2)
	G = wz[i]/np.sqrt(wx[i]**2+wy[i]**2+wz[i]**2)

	sint = np.sin(theta)
	cost = np.cos(theta)

	rotation = np.array([[A**2*(1-cost)+cost, A*B*(1-cost)-G*sint, A*G*(1-cost)+B*sint],
	[A*B*(1-cost)+G*sint, B**2*(1-cost)+cost, B*G*(1-cost)-A*sint],
	[A*G*(1-cost)-B*sint, B*G*(1-cost)+A*sint, G**2*(1-cost)+cost]])

	#for every vertex, mul by rotation matrix
	totVertices = 6*resolution**2
	newx = np.arange(totVertices, dtype='float64')
	newy = np.arange(totVertices, dtype='float64')
	newz = np.arange(totVertices, dtype='float64')
	for j in range(totVertices):
		prev = np.array([x[j], y[j], z[j]])
		newvert = prev.dot(rotation)
		newx[j] = newvert[0]
		newy[j] = newvert[1]
		newz[j] = newvert[2]

	#redraw
	ax.clear()
	ax.set_title('Torus')
	ax.set_xlabel('x (m)')
	ax.set_ylabel('y (m)')
	ax.set_zlabel('z (m)')
	axlim = 7
	ax.set_xlim(-axlim,axlim)
	ax.set_ylim(-axlim,axlim)
	ax.set_zlim(-axlim,axlim)
	ax.plot_trisurf(newx, newy, newz, triangles = np.asarray(indices))
	plt.draw()
	plt.pause(0.0001)
